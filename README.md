# L'essentiel pour programmer, avec Prof

En particulier si vous n'avez jamais programmé !

## Commencer le tutoriel

Pour cela, rien de plus simple : cliquez ici -> [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/jibe-b%2Fprof/master?urlpath=lab%2Ftree%2Fapprendre-a-programmer-en-python-avec-prof.ipynb)


## Développement

Pour une version avec les toutes dernières fonctionnalités, mais encore en cours de développement

Pour cela, rien de plus simple : cliquez ici -> [tuto-beta](https://mybinder.org/v2/gl/jibe-b%2Fprof/dev?urlpath=lab%2Ftree%2Fapprendre-a-programmer-en-python-avec-prof.ipynb)
